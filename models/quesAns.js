const mongoose = require("mongoose");

var questionSchema = new mongoose.Schema({
  questionId: {
    type: String,
    required: "question Id cannot be empty."
  },
  filterId: {
    type: String,
    required: "filter Id cannot be empty."
  },
  question: {
    type: String,
    required: "question cannot be empty."
  },
  answer: {
    type: String,
    required: "answer cannot be empty."
  },
  subjectId: {
    type: String,
    required: "Subject Id cannot be empty."
  },
  chapterId: {
    type: String,
    required: "Chapter Id cannot be empty."
  },
  topicId: {
    type: String,
    required: "Topic Id cannot be empty."
  }
});

mongoose.model("Questions", questionSchema);
