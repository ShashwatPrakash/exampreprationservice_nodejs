const mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true }, error => {
  if (!error) console.log("MongoDb connection established.");
  else console.log(`Error in MongoDb Connection ${error}`);
});

require("./user.model");
require("./semesters");
require("./subjects");
require("./subjectTopics");
require("./chapters");
require("./quesAns");
require("./filters");
