const mongoose = require("mongoose");

var filterSchema = new mongoose.Schema({
  filterId: {
    type: String,
    required: "filter Id cannot be empty."
  },
  filterName: {
    type: String,
    required: "filter name cannot be empty."
  }
});

mongoose.model("Filters", filterSchema);
