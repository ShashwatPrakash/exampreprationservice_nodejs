const mongoose = require("mongoose");

var topicsSchema = new mongoose.Schema({
  topicId: {
    type: String,
    required: "Topic Id cannot be empty."
  },
  topicName: {
    type: String,
    required: "Topic cannot be empty."
  },
  chapterId: {
    type: String,
    required: "chapter Id cannot be empty."
  },
  subjectId: {
    type: String,
    required: "Subject name cannot be empty."
  }
});

mongoose.model("Topic", topicsSchema);
