const mongoose = require("mongoose");

var subjectSchema = new mongoose.Schema({
  subjectName: {
    type: String,
    required: "subjectName cannot be empty.",
    unique: true
  },
  subjectId: {
    type: String,
    required: "subjectId cannot be empty."
  },
  semesterId: {
    type: String,
    ref: "Semester"
  }
});

mongoose.model("Subjects", subjectSchema);
