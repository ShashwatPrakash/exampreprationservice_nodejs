const mongoose = require("mongoose");

var semesterSchema = new mongoose.Schema({
  semesterName: {
    type: String,
    required: "semester cannot be empty."
  },
  semesterId: {
    type: String,
    required: "semester id cannot be empty.",
    unique: true
  }
});

mongoose.model("Semester", semesterSchema);
