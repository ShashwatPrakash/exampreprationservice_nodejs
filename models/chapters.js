const mongoose = require("mongoose");

var chapterSchema = new mongoose.Schema({
  chapterId: {
    type: String,
    required: "chapter Id cannot be empty."
  },
  chapterName: {
    type: String,
    required: "chapter name cannot be empty."
  },
  subjectId: {
    type: String,
    required: "subject Id cannot be empty."
  }
});

mongoose.model("Chapters", chapterSchema);
