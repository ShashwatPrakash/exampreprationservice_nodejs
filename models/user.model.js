const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

var userSchema = new mongoose.Schema({
  fullname: {
    type: String,
    required: "Fullname cannot be empty."
  },
  username: {
    type: String,
    required: "Username cannot be empty.",
    unique: true
  },
  password: {
    type: String,
    required: "Password cannot be empty.",
    minlength: [8, "Password must be minimum length of 8 character."]
  },
  saltSecret: {
    type: String
  },
  createdDate: {
    type: Date
  }
});

//This function run before save() method
userSchema.pre("save", function(next) {
  bcrypt.genSalt(10, (error, salt) => {
    bcrypt.hash(this.password, salt, (error, hash) => {
      this.password = hash;
      this.saltSecret = salt;
      next();
    });
  });
});

//Random Generate Password of length 8 character
userSchema.methods.generatePassword = function() {
  var length = 8,
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
};

//Password Verification at time of login
userSchema.methods.verifyPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

//Generate Token
userSchema.methods.generateToken = function() {
  return jwt.sign(
    { _id: this._id, username: this.username },
    process.env.JWT_SECRET,
    { expiresIn: process.env.JWT_EXPIRY }
  );
};
mongoose.model("User", userSchema);
