const express = require("express");
const router = express.Router();

const userController = require("../controller/user.controller");
const jwtHelper = require("../config/jwtHelper");

router.post("/registration", userController.register);
router.post("/authentication", userController.authenticate);
router.put(
  "/changePassword/:userId",
  jwtHelper.verifyJwtToken,
  userController.changePassword
);
router.post("/forgotPassword", userController.forgotPassword);

router.get(
  "/userProfile",
  jwtHelper.verifyJwtToken,
  userController.userProfile
);
router.get("/users", jwtHelper.verifyJwtToken, userController.getAllUser);
router.get(
  "/user/:userId",
  jwtHelper.verifyJwtToken,
  userController.getUserByID
);
router.delete(
  "/user/:userId",
  jwtHelper.verifyJwtToken,
  userController.deleteUser
);
router.put(
  "/user/:userId",
  jwtHelper.verifyJwtToken,
  userController.updateUser
);

router.get("/semesters", jwtHelper.verifyJwtToken, userController.getSemester);
router.get(
  "/subjects/:semesterId",
  jwtHelper.verifyJwtToken,
  userController.getSubjects
);
router.get(
  "/chapters/:subjectId",
  jwtHelper.verifyJwtToken,
  userController.getChapters
);
router.get(
  "/topics/:subjectId/:chapterId",
  jwtHelper.verifyJwtToken,
  userController.getTopics
);
router.get(
  "/queAns/:subjectId/:chapterId/:topicId",
  jwtHelper.verifyJwtToken,
  userController.getQueAns
);

router.get(
  "/filterQue/:subjectId/:chapterId/:topicId/:filterId",
  jwtHelper.verifyJwtToken,
  userController.filterque
);

router.get("/filter", jwtHelper.verifyJwtToken, userController.filter);

module.exports = router;
