const mongoose = require("mongoose");
const passport = require("passport");
const _ = require("lodash");
const sgMail = require("@sendgrid/mail");

const User = mongoose.model("User");
const Semester = mongoose.model("Semester");
const Subject = mongoose.model("Subjects");
const Chapter = mongoose.model("Chapters");
const Topic = mongoose.model("Topic");
const Question = mongoose.model("Questions");
const Filter = mongoose.model("Filters");

sgMail.setApiKey(
  "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"
);

module.exports.register = (req, res, next) => {
  var user = new User();
  user.fullname = req.body.fullname;
  user.username = req.body.username;
  user.password = req.body.password;
  user.createdDate = new Date();
  user.save((error, document) => {
    if (document) res.send(document);
    else if (error) res.send({ message: "Username already exist" });
    else next(error);
  });
};

module.exports.authenticate = (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) return res.status(400).json(err);
    else if (user) return res.status(200).json({ token: user.generateToken() });
    else return res.status(404).json(info);
  })(req, res);
};

module.exports.changePassword = (req, res, next) => {
  var passwordDetails = req.body;
  User.findById(req.params.userId, (err, user) => {
    if (user.verifyPassword(passwordDetails.currentPassword)) {
      if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
        user.password = passwordDetails.newPassword;
        user.save((error, document) => {
          if (!error)
            res.send({
              message: "Password changed successfully"
            });
          else next(error);
        });
      } else {
        res.status(400).send({
          message: "Passwords do not match"
        });
      }
    } else {
      res.status(400).send({
        message: "Current password is incorrect"
      });
    }
  });
};

module.exports.forgotPassword = (req, res, next) => {
  var input = req.body;

  User.findOne({ username: req.body.username }, (err, user) => {
    if (!user)
      return res
        .status(404)
        .send({ status: false, message: "Username not found." });
    else if (user) {
      var password = user.generatePassword();
      const msg = {
        to: input.username,
        from: "RNL@abc.com",
        subject: "RNL current password",
        text: "You password is :- ",
        html: "<strong>Your Password is :- </strong>" + password
      };
      user.password = password;
      user.save((error, document) => {
        if (!error) {
          sgMail.send(msg);
        } else next(error);
      });

      return res
        .status(200)
        .send({ status: true, message: "Email send succesfully" });
    } else return res.status(400).send(err);
  });
};

module.exports.userProfile = (req, res, next) => {
  User.findOne({ _id: req._id }, (err, user) => {
    if (!user)
      return res
        .status(404)
        .send({ status: false, message: "User record not found." });
    else
      return res.status(200).send({
        status: true,
        user: _.pick(user, ["fullname", "username"])
      });
  });
};

module.exports.getAllUser = (req, res, next) => {
  User.find((error, user) => {
    if (!error) return res.status(200).send(user);
    else return res.status(400).send(error);
  });
};

module.exports.getUserByID = (req, res, next) => {
  User.findById(req.params.userId, (error, user) => {
    if (error) return res.status(400).send(error);
    else if (!user)
      return res.status(400).send({ message: "User doesn't exist." });
    else return res.status(200).send(user);
  });
};

module.exports.deleteUser = (req, res, next) => {
  User.findByIdAndRemove(req.params.userId, (error, result) => {
    if (error) return res.status(400).send(error);
    else if (!result)
      return res.status(400).send({ message: "User doesn't exist." });
    else return res.status(200).send({ message: "User successfully deleted." });
  });
};

module.exports.updateUser = (req, res, next) => {
  User.findByIdAndUpdate(
    req.params.userId,
    req.body,
    { new: true },
    (error, user) => {
      if (error) return res.status(400).send(error);
      else return res.status(200).send(user);
    }
  );
};

module.exports.getSemester = (req, res, next) => {
  Semester.find((error, sem) => {
    if (!error) return res.status(200).send(sem);
    else return res.status(400).send(error);
  });
};

module.exports.getSubjects = (req, res, next) => {
  Subject.find({ semesterId: req.params.semesterId }, (error, subject) => {
    if (subject.length == 0)
      return res
        .status(404)
        .send({ status: false, message: "Semester doesn't exist." });
    else if (subject.length >= 1) return res.status(200).send(subject);
    else return res.status(400).send(error);
  });
};

module.exports.getChapters = (req, res, next) => {
  Chapter.find({ subjectId: req.params.subjectId }, (error, chapter) => {
    if (chapter.length == 0)
      return res
        .status(404)
        .send({ status: false, message: "Subject doesn't exist." });
    else if (chapter.length >= 1) return res.status(200).send(chapter);
    else return res.status(400).send(error);
  });
};

module.exports.getTopics = (req, res, next) => {
  Topic.find(
    { subjectId: req.params.subjectId, chapterId: req.params.chapterId },
    (error, topic) => {
      if (topic.length == 0)
        return res
          .status(404)
          .send({ status: false, message: "Chapter doesn't exist." });
      else if (topic.length >= 1) return res.status(200).send(topic);
      else return res.status(400).send(error);
    }
  );
};

module.exports.getQueAns = (req, res, next) => {
  Question.find(
    {
      subjectId: req.params.subjectId,
      chapterId: req.params.chapterId,
      topicId: req.params.topicId
    },
    (error, que) => {
      if (que.length == 0)
        return res
          .status(404)
          .send({ status: false, message: "Topic doesn't exist." });
      else if (que.length >= 1) return res.status(200).send(que);
      else return res.status(400).send(error);
    }
  );
};

module.exports.filterque = (req, res, next) => {
  Question.find(
    {
      subjectId: req.params.subjectId,
      chapterId: req.params.chapterId,
      topicId: req.params.topicId,
      filterId: req.params.filterId
    },
    (error, que) => {
      if (que.length == 0)
        return res
          .status(404)
          .send({ status: false, message: "No result found" });
      else if (que.length >= 1) return res.status(200).send(que);
      else return res.status(400).send(error);
    }
  );
};

module.exports.filter = (req, res, next) => {
  Filter.find((error, filter) => {
    if (!error) return res.status(200).send(filter);
    else return res.status(400).send(error);
  });
};
